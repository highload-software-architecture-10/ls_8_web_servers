Nginx Fine Tuning

Configure nginx that will cache only images, that were requested at least twice

Add ability to drop nginx cache by request.

You should drop cache for specific file only ( not all cache )

- Настроен Nginx сервер с кешированием картинок
- Для реализации сброса кеша используется Nginx с модулем ngx_cache_purge https://registry.hub.docker.com/r/emcniece/nginx-cache-purge
- Выполните run.sh для проверки ответов от сервера