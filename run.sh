#bash
curl -X GET -I "localhost/static/images/4.png" # MISS
curl -X GET -I "localhost/static/images/4.png" # MISS
curl -X GET -I "localhost/static/images/4.png" # HIT
curl -X GET -I "localhost/purge/static/images/4.png" # purge cache of file
curl -X GET -I "localhost/static/images/4.png" # MISS
curl -X GET -I "localhost/static/images/4.png" # MISS
curl -X GET -I "localhost/static/images/4.png" # HIT
